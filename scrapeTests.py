import unittest
from NHLScraper import NHLScraperToCSV


class TestScraper(unittest.TestCase):

	def testEvenHomeAwayGamesPlayed(self):
		scraper = NHLScraperToCSV()
		query1 = scraper.queryData('games', 'home_id == 1')
		query2 = scraper.queryData('games', 'away_id == 1')
		self.assertEqual(41, len(query1))
		self.assertEqual(41, len(query2))
		

	def testNumberOfHomeWins(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('games', 'home_wins > 53')
		self.assertEqual(1, len(query))
		

	
	def testNumberOfAwayPoints(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('games', 'away_pts > 110')
		self.assertEqual(4, len(query))
		

	
	def testGoalieWins(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('goaliePlayers', 'wins > 400')
		self.assertEqual(1, len(query))

	
	def testGoalieSaves(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('goaliePlayers', 'shotsAgainst > 20000')
		self.assertEqual(3, len(query))

	
	def testGoalieLosses(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('goaliePlayers', 'losses > 250')
		self.assertEqual(1, len(query))
	

	def testNonGoalieTopScorers(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('nonGoaliePlayers', 'points > 1000.0')
		self.assertEqual(2,len(query))
		self.assertEqual(8471214 in list(query.ix[:,0]), True)
		self.assertEqual(8471675 in list(query.ix[:,0]), True)


	def testNonGoalieBestPlusMinus(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('nonGoaliePlayers', 'plusMinus > 180')
		self.assertEqual(5, len(query))

	
	def testNonGoaliePlayer3(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('nonGoaliePlayers', 'assists > 600')
		self.assertEqual(3, len(query))

	
	def testNHLTeamID(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('teams', 'team_id == 1')
		self.assertEqual('New Jersey Devils' in list(query['team_name']), True)
		

	
	def testTotalTeams(self):
		scraper = NHLScraperToCSV()
		query = scraper.queryData('teams', 'team_id > 0')
		self.assertEqual(len(query), 31)

	




if __name__ == '__main__':
    unittest.main()