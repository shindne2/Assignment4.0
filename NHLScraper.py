import nhlscrapi #used for game data
import urllib2
import json
import pandas as pd
import csv
import os



class NHLScraperToCSV:

	def __init__(self):
		path = './Data/'
		#get Team Data
		if not os.path.isfile(path + 'NHLTeams.csv'):
			self.NHLTeamData = self.getNHLTeams()
		else:
			self.NHLTeamData = pd.read_csv(path + 'NHLTeams.csv')

		#get player data
		if not os.path.isfile(path + 'GoaliePlayers.csv') or not os.path.isfile(path + 'nonGoaliePlayers.csv'):
			self.nonGoaliePlayerCumStatData, self.GoaliePlayerCumStatData = self.getCumPlayerStats()
		else:
			self.nonGoaliePlayerCumStatData = pd.read_csv(path + 'nonGoaliePlayers.csv')
			self.GoaliePlayerCumStatData = pd.read_csv(path + 'GoaliePlayers.csv')
		
		#get game data
		if not os.path.isfile(path + 'gameStats.csv'):
			self.gameStats = self.getGameStats()
		else:
			self.gameStats = pd.read_csv(path + 'gameStats.csv')
	
		

		
	'''
	-Gets game-by-game data on teams and team stats on
		-> date of game
		-> teams playing 
		-> score of game
		-> record of teams before start of game 
		-> number of points each team has
	'''
	def getGameStats(self):
		url = 'https://statsapi.web.nhl.com/api/v1/schedule?startDate=2017-09-30&endDate=2018-04-09'
		response = urllib2.urlopen(url)
		data = json.load(response)['dates']
		game_ids = dict()
		for day in data:
			for game in day['games']:
				if 'Team ' not in game['teams']['away']['team']['name']:
					teams = game['teams']
					game_ids[game['gamePk']] = dict()
					curr_id = game_ids[game['gamePk']]
					home_team = teams['home']
					away_team = teams['away']
					curr_id['home_id'] = home_team['team']['id']
					curr_id['home_wins'] = home_team['leagueRecord']['wins']
					curr_id['home_ot'] = home_team['leagueRecord']['ot']
					curr_id['home_losses'] = home_team['leagueRecord']['losses']
					curr_id['home_pts'] = home_team['leagueRecord']['wins'] *2 + home_team['leagueRecord']['ot']
					curr_id['home_goals_scored'] = home_team['score']

					curr_id['away_id'] = away_team['team']['id']
					curr_id['away_wins'] = away_team['leagueRecord']['wins']
					curr_id['away_ot'] = away_team['leagueRecord']['ot']
					curr_id['away_losses'] = away_team['leagueRecord']['losses']
					curr_id['away_pts'] = away_team['leagueRecord']['wins'] *2 + away_team['leagueRecord']['ot']
					curr_id['away_goals_scored'] = away_team['score']

					curr_id['date'] = day['date']
					curr_id['homeWin?'] = (home_team['score'] > away_team['score'])


		game_df = pd.DataFrame(game_ids).transpose()
		game_df.to_csv(path_or_buf = './Data/gameStats.csv')

		return game_df






	'''
	-Grabs information from https://statsapi.web.nhl.com/api/v1/teams?expand=team.roster
	-Stores unique id for each team, the team name, and roster for each team by players unique id
	'''
	def getNHLTeams(self):
		response = urllib2.urlopen('https://statsapi.web.nhl.com/api/v1/teams?expand=team.roster')
		data = json.load(response)
		teams = data['teams']
		id_to_team = list()
		for team in teams:	
			team_id = list()
			team_id.append(team['id'])
			if 'Canadiens' in team['name']:
				team['name'] = 'Montreal Canadiens'
			team_id.append(team['name'])
			players = [player['person']['id'] for player in team['roster']['roster']]
			team_id.append(players)
			id_to_team.append(team_id)
			
		team_df = pd.DataFrame(id_to_team)
		team_df.columns = [ 'team_id', 'team_name', 'team_id_roster']
		team_df.to_csv(path_or_buf = './Data/NHLTeams.csv')
		
		return team_df

		
	


	'''
	-example url: https://statsapi.web.nhl.com/api/v1/people/8447400/stats?stats=statsSingleSeason&season=19811982
	-Grabs information from https://statsapi.web.nhl.com/api/v1/people/ID/stats?stats=statsSingleSeason&season=SEASON
		-> ID is playerID number
		-> SEASON is year of season in form of Y1Y1Y2Y2 (ex 2007-2008 season would be 20072008)
	-Only grabs information from 2005-2006 season to 2016-2017 season (modern expansion) (scoring has decreased significantly)
	'''
	def getCumPlayerStats(self):
		#print(self.NHLTeamData['team_id_roster'])
		nonGoalieStatValues = ['assists','timeOnIce' , 'goals', 'points', 'shots', 'games', 'hits', 'plusMinus']
		GoalieStatValues = ['wins','timeOnIce', 'losses', 'saves', 'goalsAgainst', 'shotsAgainst']
		years = range(2006,2018)[::-1]
		nonGoaliePlayers = dict()
		GoaliePlayers = dict()
		for team_roster in self.NHLTeamData['team_id_roster']:
			for player_id in team_roster:
				stats = dict()
				for year in years:
					url = 'https://statsapi.web.nhl.com/api/v1/people/' + str(player_id) + '/stats?stats=statsSingleSeason&season=' + str(year-1) + str(year)
					#print(url)
					response = urllib2.urlopen(url)
					data = json.load(response)
					player_stats = data['stats'][0]['splits']
					#no data, player wasn't in league
					if len(player_stats) == 0:
						print(url + " - contains no data")
						break

					player_stats = player_stats[0]['stat']

					
					if nonGoalieStatValues[0] in player_stats.keys():
						statValues = nonGoalieStatValues
						players = nonGoaliePlayers
					else:
						statValues = GoalieStatValues
						players = GoaliePlayers



					for statVal in statValues:
						#this stat value is not available from this URL
						if statVal not in statValues:
							print(statVal + "not in " + str(player_id) + "profile")
							break
						#specifically for the time values that need to be converted to seconds
						#and value hasnt been initialized in dictionary
						elif statVal not in stats:
							if 'time' in statVal:
								stats[statVal] = self.convertTimeToSeconds(player_stats[statVal])
							else:
								stats[statVal] = player_stats[statVal]
						#otherwise just add to remaining values
						else:
							if 'time' in statVal:
								stats[statVal] += self.convertTimeToSeconds(player_stats[statVal])
							else:
								stats[statVal] += player_stats[statVal]
				

				#players.append(stats)
				players[player_id] = stats		


		nonGoalie_df = pd.DataFrame(nonGoaliePlayers).transpose()
		nonGoalie_df.fillna(0, inplace = True)
		nonGoalie_df.to_csv(path_or_buf = './Data/nonGoaliePlayers.csv')
		Goalie_df = pd.DataFrame(GoaliePlayers).transpose()
		Goalie_df.fillna(0, inplace = True)
		Goalie_df.to_csv(path_or_buf = './Data/GoaliePlayers.csv')

		return nonGoalie_df, Goalie_df
		
	def convertTimeToSeconds(self, minsec):
			time = minsec.split(':')
			assert(len(time) == 2)
			total_time_in_secs = int(time[0]) * 60 + int(time[1])
			return total_time_in_secs

	'''
		dataset options:
			-> 'games' for game statistics
			-> 'goaliePlayers' for statistics about goalies 
			-> 'nonGoaliePlayers' for statistics about defenseman and forwards
			-> 'teams' for information about teams
		query: query statement
	'''

	def queryData(self, dataset, query):
		if dataset == 'games':
			data = self.gameStats
			return data.query(query)

		elif dataset == 'goaliePlayers':
			data = self.GoaliePlayerCumStatData
			return data.query(query)

		elif dataset == 'nonGoaliePlayers':
			data = self.nonGoaliePlayerCumStatData
			return data.query(query)


		elif dataset == 'teams':
			data = self.NHLTeamData
			return data.query(query)

		else:
			print("Invalid query")
			return None

					


#scraper = NHLScraperToCSV()
#print(scraper.queryData('nonGoaliePlayers', 'points > 1000.0'))




