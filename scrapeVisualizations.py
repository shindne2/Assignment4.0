import matplotlib.pyplot as plt
from NHLScraper import NHLScraperToCSV


def teamsWithMostGames():
	scrape = NHLScraperToCSV()
	team_data = scrape.NHLTeamData
	player_data = scrape.nonGoaliePlayerCumStatData
	id_to_team = dict()
	id_to_games_played = dict()
	for index,i in team_data.iterrows():
		id_to_team[i['team_id']] = i['team_name']
		roster = i['team_id_roster'][1:-1].split(',')
		games = 0
		for player in roster:
			if len(list(player_data[player_data['Unnamed: 0'] == int(player)]['games'])) == 1:
				games += list(player_data[player_data['Unnamed: 0'] == int(player)]['games'])[0]
		id_to_games_played[i['team_id']] = games
	
	team_games_played = list()
	for id_ in id_to_team.keys():
		team_games_played.append((id_to_team[id_], int(id_to_games_played[id_])))
	
	plt.bar(range(len(team_games_played)), [val[1] for val in team_games_played], align='center')
	plt.xticks(range(len(team_games_played)), [val[0] for val in team_games_played])
	plt.xticks(rotation=70)
	plt.show()
	
	#data[data]


def teamWithMostPoints():
	scrape = NHLScraperToCSV()
	team_data = scrape.NHLTeamData
	player_data = scrape.nonGoaliePlayerCumStatData
	id_to_team = dict()
	id_to_games_played = dict()
	for index,i in team_data.iterrows():
		id_to_team[i['team_id']] = i['team_name']
		roster = i['team_id_roster'][1:-1].split(',')
		points = 0
		for player in roster:
			if len(list(player_data[player_data['Unnamed: 0'] == int(player)]['games'])) == 1:
				points += list(player_data[player_data['Unnamed: 0'] == int(player)]['games'])[0]
		id_to_games_played[i['team_id']] = points
	
	team_games_played = list()
	for id_ in id_to_team.keys():
		team_games_played.append((id_to_team[id_], int(id_to_games_played[id_])))
	
	plt.bar(range(len(team_games_played)), [val[1] for val in team_games_played], align='center')
	plt.xticks(range(len(team_games_played)), [val[0] for val in team_games_played])
	plt.xticks(rotation=70)
	plt.show()


teamsWithMostGames()
teamWithMostPoints()


